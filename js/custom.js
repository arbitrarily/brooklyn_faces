(function($) {

    // Fire up Foundation
    $(document).on('ready', function() {
        $(document).foundation();
    });

    // The Modal
    $('.social a, .x').on('click touch', function(e) {
    	e.preventDefault();
    	$('.sign-up').fadeToggle();
    });

    // Toggle Play Styling
    $('.content img').on('click touch', function() {
    	if ($(this).parent().hasClass('active')) {
            console.log($(this));
            $(this).parent().removeClass('active').siblings().removeClass('active');
        } else {
            $(this).parent().addClass('active').siblings().removeClass('active');
        }
    });

})(jQuery);

// Audio
function playSound(id) {
    var audio = document.getElementById(id);
    var beat_001 = document.getElementById('beat_001');
    var beat_002 = document.getElementById('beat_002');
    var beat_003 = document.getElementById('beat_003');
    var beat_004 = document.getElementById('beat_004');

    if (audio.paused) {
        audio['currentTime'] = 0;
        beat_001.pause();
        beat_002.pause();
        beat_003.pause();
        beat_004.pause();
        audio.play();
    } else {
        audio.pause();
    }
}

// Typekit
(function(d) {
    var config = {
            kitId: 'vpy0hlf',
            scriptTimeout: 3000
        },
        h = d.documentElement,
        t = setTimeout(function() {
            h.className = h.className.replace(/\bwf-loading\b/g, "") + " wf-inactive";
        }, config.scriptTimeout),
        tk = d.createElement("script"),
        f = false,
        s = d.getElementsByTagName("script")[0],
        a;
    h.className += " wf-loading";
    tk.src = '//use.typekit.net/' + config.kitId + '.js';
    tk.async = true;
    tk.onload = tk.onreadystatechange = function() {
        a = this.readyState;
        if (f || a && a != "complete" && a != "loaded") return;
        f = true;
        clearTimeout(t);
        try {
            Typekit.load(config)
        } catch (e) {}
    };
    s.parentNode.insertBefore(tk, s)
})(document);