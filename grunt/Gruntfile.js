///////////////////////////////////////
// Grunt Config
///////////////////////////////////////

module.exports = function(grunt) {
    grunt.initConfig({

        // Load package.json
        pkg: grunt.file.readJSON('package.json'),

        // SASS
        sass: {
            options: {
                loadPath: ['bower_components/foundation/scss']
            },
            dist: {
                options: {
                    sourcemap: 'none',
                    style: 'nested'
                },
                files: [{
                    expand: true,
                    cwd: '../sass',
                    src: ['*.scss'],
                    dest: '../sass',
                    ext: '.css'
                }]
            }
        },

        // Concat
        concat: {
            options: {
                separator: ';'
            },
            script: {
                src: [
                    'bower_components/jquery/dist/jquery.js',
                    'bower_components/foundation/js/foundation/foundation.js',
                    '../js/custom.js'
                ],
                dest: '../js/script.js'
            },
            modernizr: {
                src: [
                    'bower_components/modernizr/modernizr.js',
                ],
                dest: '../js/modernizr.js'
            }
        },

        // Uglify
        uglify: {
            dist: {
                files: {
                    '../js/jquery.min.js': ['bower_components/jquery/dist/jquery.js'],
                    '../js/modernizr.min.js': ['../js/modernizr.js'],
                    '../script.min.js': ['../js/script.js']
                }
            }
        },

        // CSS Min
        cssmin: {
            dist: {
                src: ['../sass/styles.css'],
                dest: '../style.min.css'
            }
        },

        // Watch
        watch: {
            grunt: {
                files: ["Gruntfile.js"],
                tasks: ["default"]
            },
            sass: {
                files: "../sass/**/*.scss",
                tasks: ["default"]
            },
            script: {
                files: '../js/custom.js',
                tasks: ['default']
            }
        },

    });

    // Register Task
    grunt.registerTask('default', ['sass', 'concat', 'uglify', 'cssmin', 'watch']);

    // Load Tasks
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

}
